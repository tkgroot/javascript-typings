import { add, adds, lowestPrime } from "./utils";

let res: number | undefined;
let arr: Array<number>;

res = add(1, "4");
console.log(res);

arr = adds("4", [1, 2, 3]);
console.log(arr);

res = adds("4", [1, 2, 3])
  .map((n) => n + 1)
  .find(lowestPrime);
console.log(res);
