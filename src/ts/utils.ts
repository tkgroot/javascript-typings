/**
 * adds two terms together
 * @param a first term
 * @param b second term
 * @returns sum of the two terms
 */
export const add = (a: number, b: string): number => {
  return a + +b;
};

/**
 * adds a number to an array
 * @param num number to add
 * @param arr array
 * @returns
 */
export const adds = (num: string, arr: Array<number>): Array<number> => {
  arr.push(+num);
  return arr;
};

/**
 *
 * @param n an arbitrary number
 * @returns
 */
export const lowestPrime = (n: number): boolean => n === 2;
