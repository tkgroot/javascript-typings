// @ts-ignore
const { add, adds, lowestPrime } = require("./utils.js");
/**
 * @type {number | undefined}
 * @const
 */
let res;
/**
 * @type {Array<number>}
 * @const
 */
let arr;

res = add(1, "4");
console.log(res);

arr = adds("4", [1, 2, 3]);
console.log(arr);

res = adds("4", [1, 2, 3])
  .map((n) => n + 1)
  .find(lowestPrime);
console.log(res);
