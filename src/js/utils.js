/**
 * checks if variable is of type String
 * @param {string} str - variable to validate
 */
const shouldBeString = (str) => {
  if (typeof str !== "string") throw TypeError("Wrong type!");
  return;
};

/**
 * adds two terms together
 * @param {number} a first term
 * @param {string} b second term
 * @returns {number} sum of the two terms
 */
const add = (a, b) => {
  shouldBeString(b);
  return a + +b;
};

/**
 * adds a new item to an array
 * @param {string} num number
 * @param {Array<number>} arr a number array
 * @returns {Array<number>}
 */
const adds = (num, arr) => {
  shouldBeString(num);
  arr.push(+num);
  return arr;
};

/**
 * checks if number is the lowest prime number
 * @param {number} n the number to validate
 * @returns {boolean}
 */
const lowestPrime = (n) => n === 2;

module.exports = {
  add,
  adds,
  lowestPrime,
};
