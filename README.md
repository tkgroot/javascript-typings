# Readme

Even though Javascript doesn't offer type checks by default, one can leverage the typescript
compiler in combination with JSDoc to generate type safe documentation of your code in vanillaJS.

This repository shows with simple examples why it is useful to document your function, which
benefits documentation can have on your repository and the benefit of type checks and type safety
by leveraging the typescript compiler to check and validate Javascript code.

## Scripts

The repository offers a couple of npm run-scripts to be executed.

| scripts  | description                                           |
| -------- | ----------------------------------------------------- |
| clean    | removes a given file or directory                     |
| doc      | generates documentation using jsdoc                   |
| exec     | executes the index.js file                            |
| exec:ts  | executes the index.ts file                            |
| format   | runs code and file formatting                         |
| lint     | runs code linters                                     |
| serve    | serves the generated documentation                    |
| start    | starts the javascript example file with file watching |
| start:ts | starts the typescript example file with file watching |
| tsc      | complies typescript                                   |

## Contributing

I don't expect any contributions to this repository. If you want to contribute, file an issue via
the issues and wait.
